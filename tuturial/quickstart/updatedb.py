from .models import Post, Word
from bs4 import BeautifulSoup
from collections import OrderedDict
from operator import itemgetter
from requests import get
from django.db.models import Count

teonite_url = 'https://teonite.com'

def updateDB():
    updatePost()
    updateWord()
    
def parseHTML(url):
    return BeautifulSoup(get(url).content, 'html.parser')

def updateWord():
    Word.objects.all().delete()
    words_count = {}
    authors = Post.objects.values('author').annotate(author_count=Count('author'))

    for row in authors:

        posts = Post.objects.filter(author=row['author']).order_by('published_date')

        for post in posts:
            words = post.text.split()
            for word in words:
                word = word.lower()
                if word in words_count.keys():
                    words_count[word] += 1
                else:
                    words_count.update({word:1})


        words_sorted = sorted(words_count.items(), key = itemgetter(1), reverse = True)

        max_in_db = 10
        in_db = 0

        for word in words_sorted:
            newWord = Word(author = row['author'], word = word[0], count = word[1])
            newWord.save()
            in_db += 1
            if in_db >= max_in_db:
                break

def updatePost():

    Post.objects.all().delete()

    #download post to database
    page_no = 0
    keep_do = 1

    while keep_do:

        page_no += 1

        blog_post_list_url = teonite_url + '/blog/page/' + str(page_no) + '/';

        blog_post_list = parseHTML(blog_post_list_url)

        test = blog_post_list.title.get_text();

        articles = blog_post_list.find_all('article');

        if len(articles) == 0:
            break

        for child in articles:
            blogDetailUrl = child.find(attrs={"class":"read-more"}).get('href')
            blogDetail =  parseHTML(teonite_url + blogDetailUrl)

            http_author = blogDetail.find(attrs={"class":"author-content"}).h4.get_text().encode('utf-8')
            http_title = blogDetail.find(attrs={"class":"post-title"}).get_text().encode('utf-8')
            http_content = blogDetail.find(attrs={"class":"post-content"}).get_text().encode('utf-8')

            newPost = Post(title = http_title, text = http_content, author = http_author)
            newPost.save()
            #break #only first post for test
        #break #olny first page for tests
