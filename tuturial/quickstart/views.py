from django.contrib.auth.models import User
from rest_framework import viewsets
from tuturial.quickstart.serializers import WordSerializer
from .updatedb import updateDB
from rest_framework.decorators import api_view
from .models import Word
from rest_framework import generics
from rest_framework.response import Response
from rest_framework import mixins
from rest_framework import generics
from django.db.models import Sum


#updateDB()

class Stats(generics.ListCreateAPIView):
    queryset = Word.objects.values('word').annotate(count=Sum('count'))[0:10]
    serializer_class = WordSerializer

@api_view(['GET'])
def statsForAuthor(request, pk):
    words = Word.objects.filter(author=pk).order_by('-count')
    res = {}
    for word in words:
        res.update({word.word:word.count})

    return Response(res)

"""
@api_view(['GET'])
def statsForAuthor(request, pk):
    words = Word.objects.filter(author=pk).order_by('-count')
    serializer = WordSerializer(words, many=True)
    return Response(serializer.data)
"""
